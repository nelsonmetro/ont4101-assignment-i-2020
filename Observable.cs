/**
 * This class represents an observable object, or "data"
 * in the model-view paradigm. It can be subclassed to represent an
 * object that the application wants to have observed.
 * <p>
 * An observable object can have one or more observers. An observer
 * may be any object that implements interface <tt>Observer</tt>. After an
 * observable instance changes, an application calling the
 * <code>Observable</code>'s <code>NotifyObservers</code> method
 * causes all of its observers to be notified of the change by a call
 * to their <code>Update</code> method.
 * <p>
 * The order in which notifications will be delivered is unspecified.
 * The default implementation provided in the Observable class will
 * notify Observers in the order in which they registered interest, but
 * subclasses may change this order, use no guaranteed order, deliver
 * notifications on separate threads, or may guarantee that their
 * subclass follows this order, as they choose.
 * <p>
 * Note that this notification mechanism has nothing to do with threads
 * and is completely separate from the <tt>Wait</tt> and <tt>Notify</tt>
 * mechanism of class <tt>object</tt>.
 * <p>
 * When an observable object is newly created, its set of observers is
 * empty. Two observers are considered the same if and only if the
 * <tt>equals</tt> method returns true for them.
 *
 * @author  Chris Warth
 * @see     root.Observable#notifyObservers()
 * @see     root.Observable#NotifyObservers(object)
 * @see     root.Observer
 * @see     root.Observer#Update(root.Observable, object)
 * @since   JDK1.0
 */

using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace root
{
    class Observable
    {
        private bool changed = false;
        private List<IObserver> obs;

        /** Construct an Observable with zero Observers. */
        public Observable()
        {
            obs = new List<IObserver>();
        }

     /**
     * Adds an observer to the set of observers for this object, provided
     * that it is not the same as some observer already in the set.
     * The order in which notifications will be delivered to multiple
     * observers is not specified. See the class comment.
     *
     * @param   o   an observer to be added.
     * @throws NullReferenceException if the parameter o is null.
     */
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void AddObserver(IObserver o)
        {
            if (o == null) throw new System.NullReferenceException();
            if (!obs.Contains(o)) obs.Add(o);
        }

        /**
         * Deletes an observer from the set of observers of this object.
         * Passing <CODE>null</CODE> to this method will have no effect.
         * @param   o   the observer to be deleted.
         */
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void RemoveObserver(IObserver o)
        {
            obs.Remove(o);
        }

        /**
         * If this object has changed, as indicated by the
         * <code>hasChanged</code> method, then notify all of its observers
         * and then call the <code>clearChanged</code> method to
         * indicate that this object has no longer changed.
         * <p>
         * Each observer has its <code>update</code> method called with two
         * arguments: this observable object and <code>null</code>. In other
         * words, this method is equivalent to:
         * <blockquote><tt>
         * notifyObservers(null)</tt></blockquote>
         *
         * @see     root.Observable#ClearChanged()
         * @see     root.Observable#HasChanged()
         * @see     root.Observer#Update(root.Observable, object)
         */
        public void NotifyObservers()
        {
            NotifyObservers(null);
        }

        /**
     * If this object has changed, as indicated by the
     * <code>HasChanged</code> method, then notify all of its observers
     * and then call the <code>ClearChanged</code> method to indicate
     * that this object has no longer changed.
     * <p>
     * Each observer has its <code>Update</code> method called with two
     * arguments: this observable object and the <code>arg</code> argument.
     *
     * @param   arg   any object.
     * @see     root.Observable#clearChanged()
     * @see     root.Observable#hasChanged()
     * @see     root.Observer#Update(root.Observable, object)
     */
        public void NotifyObservers(object arg)
        {
            /*
             * a temporary array buffer, used as a snapshot of the state of
             * current Observers.
             */
            object[] arrLocal;

            lock (this)
            {
                /* We don't want the Observer doing callbacks into
                 * arbitrary code while holding its own Monitor.
                 * The code where we extract each Observable from
                 * the Vector and store the state of the Observer
                 * needs synchronization, but notifying observers
                 * does not (should not).  The worst result of any
                 * potential race-condition here is that:
                 * 1) a newly-added Observer will miss a
                 *   notification in progress
                 * 2) a recently unregistered Observer will be
                 *   wrongly notified when it doesn't care
                 */
                if (!changed) return;
                arrLocal = obs.ToArray();
                ClearChanged();
            }
            for (int i = 0; i < arrLocal.Length; i++) ((IObserver)arrLocal[i]).Update(this, arg);
        }

	/**
	* Clears the observer list so that this object no longer has any observers.
	*/
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void RemoveObservers()
        {
            obs.Clear();
        }

	/**
	* Marks this <tt>Observable</tt> object as having been changed; the
	* <tt>HasChanged</tt> method will now return <tt>true</tt>.
	*/
        [MethodImpl(MethodImplOptions.Synchronized)]
        protected void SetChanged()
        {
            changed = true;
        }

	/**
	* Indicates that this object has no longer changed, or that it has
	* already notified all of its observers of its most recent change,
	* so that the <tt>HasChanged</tt> method will now return <tt>false</tt>.
	* This method is called automatically by the
	* <code>NotifyObservers</code> methods.
	*
	* @see     root.Observable#notifyObservers()
	* @see     root.Observable#notifyObservers(object)
	*/
        [MethodImpl(MethodImplOptions.Synchronized)]
        protected void ClearChanged()
        {
            changed = false;
        }

	/**
	* Tests if this object has changed.
	*
	* @return  <code>true</code> if and only if the <code>SetChanged</code>
	*          method has been called more recently than the
	*          <code>ClearChanged</code> method on this object;
	*          <code>false</code> otherwise.
	* @see     root.Observable#ClearChanged()
	* @see     root.Observable#SetChanged()
	*/
        [MethodImpl(MethodImplOptions.Synchronized)]
        public bool HasChanged()
        {
            return changed;
        }

	/**
	* Returns the number of observers of this <tt>Observable</tt> object.
	*
	* @return  the number of observers of this object.
	*/
        [MethodImpl(MethodImplOptions.Synchronized)]
        public int CountObservers()
        {
            return obs.Count;
        }
    }
}
