namespace root
{
	class Burrito : Ingredient
	{
		public override string GetName()
		{
			return "Burrito";
		}
		public override string GetDescription()
		{
			return "Burrito";
		}
		public override double GetCost()
		{
			return 45;
		}
	}
}
