namespace root
{
	class Taco : Ingredient
	{
		public override string GetName()
		{
			return "Taco";
		}
		public override string GetDescription()
		{
			return "Taco";
		}
		public override double GetCost()
		{
			return 40;
		}
	}
}
