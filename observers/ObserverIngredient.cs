using System;

namespace root
{
    class ObserverIngredient : IObserver, IDisplay
    {
        private Ingredient ingredient;
        private Observable observable;

        public ObserverIngredient(Observable observable)
        {
            this.observable = observable;
            observable.AddObserver(this);
        }
        public void Update(Observable obs, object arg)
        {
            if (obs is Subject)
            {
                Subject subject = (Subject)obs;
                this.ingredient = subject.GetIngredient();
                Display();
            }
        }
        public void Display()
        {
            string order = "Preparing to serve " + this.ingredient.GetDescription();
            string[] items = order.Split(',');
	    order = items[0] + " (" + this.ingredient.GetCost().ToString("C") + ")";
            switch (items.Length)
            {
                case 1:
                    break;
		case 2:
                    order += "\nOrder includes " + items[1];
		    break;
                default:
                    order += "\nOrder includes following;\n* " + items[1];
                    for (byte i = 2; i < items.Length; i++) order += "\n* " + items[i];
                    break;
            }
            Console.WriteLine("{0}\n", order);
        }
    }
}
