namespace root.toppings
{
    abstract class Poultry : IngredientDecorator
    {
		Ingredient ingredient;
		public Poultry(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
    }
}
