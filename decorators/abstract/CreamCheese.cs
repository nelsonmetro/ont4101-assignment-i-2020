namespace root.toppings
{
    abstract class CreamCheese : IngredientDecorator
    {
		Ingredient ingredient;
		public CreamCheese(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
    }
}
