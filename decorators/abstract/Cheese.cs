namespace root.toppings
{
    abstract class Cheese : IngredientDecorator
    {
		Ingredient ingredient;
		public Cheese(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
    }
}
