using root.toppings;
namespace root.decorators
{
	class Habanero : Chili
	{
		Ingredient ingredient;
		public Habanero(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Habanero Chilies";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Habanero Chilies";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 8;
		}
	}	
}
