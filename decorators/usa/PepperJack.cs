using root.toppings;
namespace root.decorators
{
	class PepperJack : Cheese
	{
		Ingredient ingredient;
		public PepperJack(Ingredient ingredient) :base(ingredient)
		{
			this.ingredient = ingredient;
		}
		public override string GetName ()
		{
			return "Pepper Jack Cheese";
		}
		public override string GetDescription ()
		{
			return ingredient.GetDescription() + ", Pepper Jack Cheese";
		}
		public override double GetCost ()
		{
			return ingredient.GetCost() + 5;
		}
	}	
}
