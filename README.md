# Coolie Kitchen
## Observer Pattern
This pattern is responsible for updating the user with the current state of the products ordered (the ingredient and the complementary toppings).  
This patterns also provide a device to assume the user's role when interacting with application (**Subject** class instance).  
![UML Class](https://cloud.owncube.com/apps/gallery/preview.public/52459387?width=1400&height=1400&c=4068f70b803c2f54bfbefa6accd1dcc2&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)
## Decorator Pattern
This pattern is responsible for compositing the *'flagship product'* (ingredient), with the *'complementary products'* (toppings).  


This pattern also introduces the following;-  
* The two flagship product classes (**Burrito** & **Taco**), serving as **concrete components** to pattern  
* Abstraction of the complementary products (**Poultry**, **Meat**, etc), serving as second generation to the **abstract decorator**.  
![UML Class](https://cloud.owncube.com/apps/gallery/preview.public/52459385?width=1400&height=1400&c=9c5e730f815e5cec423465bda7446039&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)
## Factory Method
This pattern provides an implementation to the abstract class of one of the **Subject** class members (**Kitchen**).  
This implementation servers as a **Client** to the **Abstract Factory** pattern.  

Furthermore, it implements the method responsible for instantiating a declaration of the flagship product (**Ingredient**) instance.  
![UML Class](https://cloud.owncube.com/apps/gallery/preview.public/52459386?width=1400&height=1400&c=adc28ed27033ee067539c688211247d9&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)
## Abstract Factory
This pattern provides a mechanism to instantiate declarations of the complementary product (**topping**) classes with the correct constructors, based on the context.  
These instances are then used to *decorate* the flagship product (**ingredient**).  
![UML Class](https://cloud.owncube.com/apps/gallery/preview.public/52459383?width=1400&height=1400&c=2e3563e59cfb3f857d9582b2e25835cd&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)  
# Solution
The solution combines all four of the above design patterns.  

In the context of the business (Coolie Kitchen), this solution have **products** divided into two categories; the _flagship products_ (the **ingredient**s), and the _complementary products_ (the **topping**s).  
Each **Kitchen** has its own **abstract factory** to construct **toppings**, relevant to that region, to **decorate** the **Ingredient**.  

## Diagram Explained
The final version of the UML Class Diagram utilizes **colour coding**.  
The following table isolates classes as time when they were introduced into the picture.   

|	Design Pattern	| Colour	|
|-------------------|-----------|
| Observer			| Green		|
| Decorator			| Orange	|
| Factory Method	| Cyan		|
| Abstract Factory	| Magenta	|  
Concrete products of the **Abstract Factory** pattern have been given special formatting, since they are also concrete decorators of the **Decorator** pattern.  

![UML Class](https://cloud.owncube.com/apps/gallery/preview.public/52459378?width=1400&height=1400&c=9f0b0d92297a81ce3f23acccd3012f33&requesttoken=qs965siX9tD2NnnXrhrU2ZYbectwGC2cNkzY4%2Fx7rAw%3D%3Ay4IP0q%2B4wL6wVyrum1y4v8UtSIgdTH3FRSqKpJIt6Do%3D&token=7dLtPm4wPGZw9HM)  


